/* TODO:
 *  - Get rid of _prev
 *  - Implement pausing (rather than just stopping)
 *  - Implement logging backend
 *  - Implement day splitting (i.e. split over to the next day at midnight)
 *  - Implement logging frontend (graph showing how much work has been done and
 *    when it was done)
*/
var button = null;
var time_cells = null;
var clock_anim_id = null;
var time_cell_num = 9;
var timer_start = false;

function get_unix_time() {
    /* we don't use UTC because local time is important for continuity with days
       (we plan on resetting the counter each day and logging the previous day's
        work, and this wouldn't make sense if it worked on UTC) */
    return new Date().getTime() / 1000;
}

function Time(s) {
    this.millis = Math.floor((s % 1) * 1000);
    this.seconds = Math.floor(s % 60);
    s = Math.floor(s / 60);
    this.minutes = s % 60;
    s = Math.floor(s / 60);
    this.hours = s;
}

function split_to_cells(num, cells) {
    var rv = new Array(cells);
    for (var i = 1; i <= cells; i++) {
        rv[cells - i] = num % 10;
        // shift
        num = Math.floor(num / 10);
    }
    return rv;
}

function to_cells(t) {
    return [].concat(split_to_cells(t.hours, 2),
            split_to_cells(t.minutes, 2),
            split_to_cells(t.seconds, 2),
            split_to_cells(t.millis, 3));
}

function clock_update(prev) {
    var current_time = get_unix_time();
    var diff = new Time(current_time - timer_start);
    // TODO: we can do this with less DOM manipulation (if we diff the current
    // and previous times), but would that make this operation faster?
    var cells = to_cells(diff);
    for (var i = time_cell_num - 1; i >= 0; i--) {
        time_cells[i].innerHTML = cells[i];
    }
    _prev = current_time;
}

// TODO: instead of using globals, try creating an "updater object" with
// internal state
var _prev = undefined;
function clock_update_wrapper() {
    clock_update(_prev);
}

function get_state() {
    var d = window.localStorage["studytiem:clockstart"];
    if (d === undefined) return undefined;
    return parseFloat(d);
}

function init_timers() {
    /* synchronises localStorage model with the browser */
    var start_time = get_state();
    if (start_time !== undefined) {
        button.classList.add("running");
        button.classList.remove("paused");
        button.innerHTML = "pause";
        _prev = get_unix_time();
        timer_start = start_time;
        clock_anim_id = window.setInterval(clock_update_wrapper, 33);
    }
    else {
        button.classList.add("paused");
        button.classList.remove("running");
        button.innerHTML = "start";
        timer_start = undefined;
        window.clearInterval(clock_anim_id);
        _prev = undefined;
    }
}

function start_stop() {
    if (get_state() === undefined) {
        window.localStorage["studytiem:clockstart"] = get_unix_time();
    }
    else {
        delete window.localStorage["studytiem:clockstart"];
    }
    // init timers and button
    init_timers();
}

window.onload = function() {
    button = document.querySelector("#startstop");
    time_cells = document.querySelectorAll(".digit");
    button.onclick = start_stop;
    window.onkeypress = function(k) {
        // 32 == space
        if (k.charCode === 32) start_stop();
    };
    init_timers();
}
